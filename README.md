# React test

### Hello!

Thank you very much for taking our test. It is nothing too technical so don't worry!
I just would like to see the way you approach a problem, and your style when using React.


### The premise

Hello, my name is James. I have been working at Goss for a while now, and they have just asked me
to create a messaging app for them. I have made a start on it, but as you'll see I am
not doing very well at all! I have made a lot of mistakes and I'm worried I won't be able
complete the task. Worst of all, it needs to be done in the next hour!!!

**But no need to worry!!!!**
As Goss's new hotshot developer, I know you will be able to get me out of this mess!

You have one hour to:
- Create a branch with the following naming structure: [forename]\_[surname] 
- Refactor the code so that it is implemented in a more React-ish way.
- Send messages to the server with the supplied url, and visually mark them as 'sent' when you receive the response.
- Make sure the messages appear in the correct order - be careful, the server code I wrote is laggy!


### Tips

- Please try and show good code practices throughout.
- If you have time left over you are more than welcome to show off a bit.
- Use whatever Libs you want.
- Feel free to leave comments to explain anything you want to highlight.
- Feel free to leave TODOs, but try to use language that shows you also know how to implement the suggestion.
- No need to comment if you have removed an existing bug - I will see what has been fixed..

### Recources

The URL you will need in order to send messages is:

```bash
https://jamestrickey.com/send-message 
```

Request body:
```js
{
  localId, // String - Id created on machin
  message, // String - the messge itself
}
```

Response body
```js
{
  localId,  // String - Id created by client
  messge,   // String - the message text
  id,       // String - id from the server
  isSent,   // Boolean - to check message is sent (always true in test)
}
```


# GOOD LUCK!
