import {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import { v4 } from 'uuid';

const SEND_MESSAGE_URL = "https://jamestrickey.com/send-message";

const createMessage = (message, localId, isSeen = false) => ({
  localId,
  message,
  isSeen
})

const messages = [ createMessage('Hi!', v4() ), createMessage('hello!', v4() ) ];

function App() {
  const [, refresh] = useState({});
  return (
    <div className="App">
      <div className="App-message-list" >
        { messages.map(({message}, index) => <Message message={message} index={index} />) }
      </div>
      <MessageForm refresh={() => refresh({})}  />
    </div>
  )
}

const Message = (props) => {
  const {message, index} = props;
  return (
    <div className="Message-holder">
      <p>Message {index + 1}: {message}</p> 
    </div>
  )
}

let currentMessageValue="";
const MessageForm = (props) => {
  const { refresh } = props;

  const onSubmit = (event) => {
    event.preventDefault(); 
    messages.push(createMessage(currentMessageValue, v4()));
    refresh();
  };

  const onChange = (event) => {
    currentMessageValue = event.target.value;
  }

  return (
    <form className="MessageForm" onSubmit={onSubmit}>
      <label>
        Enter message:
        <input  onChange={onChange} type="type" />
      </label>
      <input type="submit" value="Send" />
    </form> 
  )
}

export default App;
